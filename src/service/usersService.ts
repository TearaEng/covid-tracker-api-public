//Import everything needed for the user pipeline
import {logger} from '../middleware/winston'
import * as usersRepository from '../repository/usersRepository'
import {users} from '../model/models'

//a place for more complexity, but simply passes on the data to the repository 
export async function findAllUsers(){
    logger.info(`User Service layer`);
    return usersRepository.findAllUsers();
}

export async function createNewUser(input:users){
    logger.info(`User Service layer`);
    return usersRepository.createNewUser(input);
}

export async function findByState(input:users){
    logger.info(`User Service layer`);
    return usersRepository.findByState(input);
}