//Import everything needed for the covid pipeline
import {logger} from '../middleware/winston'
import * as covidRepository from '../repository/covidRepository'
import {covid} from '../model/models'

//a place for more complexity, but simply passes on the data to the repository 
export async function findOneCovid(input:covid){
    logger.info(`Covid Service layer`);
    return covidRepository.findOneCovid(input);
}