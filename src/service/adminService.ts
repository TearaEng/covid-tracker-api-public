//Import everything needed for the admin pipeline
import {logger} from '../middleware/winston'
import * as adminRepository from '../repository/adminRepository'
import {users,covid} from '../model/models'

//a place for more complexity, but simply passes on the data to the repository 
export async function updateOneCovid(input:covid){
    logger.info(`Admin Service layer`);
    return adminRepository.updateOneCovid(input);
}

export async function deleteOneUser(input:users){
    logger.info(`Admin Service layer`);
    return adminRepository.deleteOneUser(input);
}