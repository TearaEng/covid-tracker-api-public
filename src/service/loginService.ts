//Import everything needed for the login pipeline
import * as loginRepository from '../repository/loginRepository'
import {users} from '../model/models'
import {logger} from '../middleware/winston'

//a place for more complexity, but simply passes on the data to the repository 
export async function checkLogin(input:users){
    logger.info(`Login Service layer`);
    return loginRepository.checkLogin(input);
}