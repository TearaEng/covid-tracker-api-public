const request = require('supertest')
const app = require('../index')

//Checks userRouter
describe('User Router Test', () => {

  //A basic test that just checks if successfully returns something
  it('/getAll Test', async () => {
    const res:any = await request(app)
    .get('/users/getAll');

    expect(res.statusCode).toEqual(200)
  })

  //A test that checks the objects with specific elements are returned
  it('/getByState Test', async () => {
    const res:any = await request(app)
    .get('/users/getByState')
    .send({
      user_state: "Texas"
    });

    let teara = {"full_name": "Teara Eng", "health_status": "healthy", "user_role": "Admin", "user_state": "Texas"};
    let paul = {"full_name": "Paul Phoenix", "health_status": "infected", "user_role": "Guest", "user_state": "Texas"};

    expect(res.statusCode).toEqual(200)
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(paul), 
        expect.objectContaining(teara)
      ])
    )
  })
})

//Tests the covidRouter endpoint
describe('Covid Router Test', () => {

  //Checks that the array of objects whose elements can have any value is returned 
  it('/getOne Test', async () => {
    const res:any = await request(app)
    .get('/covid/getOne')
    .send({
      covid_state_name: "Texas"
    });

    expect(res.statusCode).toEqual(200)
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          confirmed_cases: expect.any(Number),
          covid_state_name: 'Texas',
          deaths: expect.any(Number),
          recoveries: expect.any(Number)
        })
      ])
    )
  })
})

//Tests the loginRouter endpoint
describe('Login Router Test', () => {
  it('/login Test', async () => {
    const res:any = await request(app)
    .post('/auth/login')
    .send({
      "email": "TimHwo@gmail.com",
      "user_password": "talonkicks"
    });

    expect(res.statusCode).toEqual(200)
  })

  //
  it('/logout Test', async () => {
    
    const res:any = await request(app)
    .get('/auth/logout');

    expect(res.statusCode).toEqual(200)
  })

  //
  it('/check Test', async () => {
    
    const res:any = await request(app)
    .get('/auth/check');

    expect(res.statusCode).toEqual(200)
    expect(res.body).toMatchObject({})
  })
})

//fix thisss
// describe('Admin Router Test', () => {
//   it('/updateOneCovid Test', async () => {
    
//     await request(app)
//     .post('/auth/login')
//     .send({
//       "email": "TimHwo@gmail.com",
//       "user_password": "talonkicks"
//     })

//     const res:any = await request(app)
//     .put('/admin/updateOneCovid')
//     .send({
//       "covid_state_name": "Texas", 
//       "confirmed_cases": "48693", 
//       "deaths": "1347", 
//       "recoveries": "28371"
//     });

//     expect(res.statusCode).toEqual(200)
//     expect(res.body).toEqual("Texas has been updated!")
//   })
// })