import {users, states, covid} from "../model/models"

export class usersDTO{
    full_name: string;
    user_role: string;
    user_password: string;
    user_state: string;
    health_status: string;
    address: string;
    email: string;
    id: number;
    phone_number: string;
    constructor(full_name: string, user_role: string, user_password: string, user_state: string, health_status: string, address: string, email: string, id: number, phone_number: string){
        this.full_name = full_name;
        this.user_role = user_role;
        this.user_password = user_password;
        this.user_state = user_state;
        this.health_status = health_status;
        this.address = address;
        this.email = email;
        this.id = id;
        this.phone_number = phone_number;
    }
}

export class statesDTO{
    state_name: string;
    population: number;
    state_size_sq_mi: number;
    density: number;
    constructor(state_name: string, population: number, state_size_sq_mi: number, density: number){
        this.state_name = state_name;
        this.population = population;
        this.state_size_sq_mi = state_size_sq_mi;
        this.density = density;
    }
}

export class covidDTO{
    covid_state_name: string;
    confirmed_cases: number;
    deaths: number;
    recoveries: number;
    constructor(covid_state_name: string, confirmed_cases: number, deaths: number, recoveries: number){
        this.covid_state_name = covid_state_name;
        this.confirmed_cases = confirmed_cases;
        this.deaths = deaths;
        this.recoveries = recoveries;
    }
}

//Used to convert objects from the database to objects in the server
export function convertToUsersArray(input:usersDTO):users{
    const newUsers = new users(input.full_name, input.user_role, input.user_password, input.user_state, input.health_status, input.address, input.email, input.id, input.phone_number);
    return newUsers;
}

export function convertToStatesArray(input:statesDTO):states{
    const newStates = new states(input.state_name, input.population, input.state_size_sq_mi, input.density);
    return newStates;
}

export function convertToCovidArray(input:covidDTO):covid{
    const newCovid = new covid(input.covid_state_name, input.confirmed_cases, input.deaths, input.recoveries);
    return newCovid;
}