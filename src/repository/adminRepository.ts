//import everything needed for the admin pipeline
import {logger} from '../middleware/winston'
import {users,covid} from "../model/models"
import {pool} from "./database"
import {convertToCovidArray, convertToUsersArray} from "../dto/DTO"
require('dotenv').config()


//Update covid status 
export async function updateOneCovid(input:covid){

    let client;
    try{
        client = await pool.connect();
        logger.info('Connected to database')
    
        const results = await client.query('UPDATE covid SET confirmed_cases = $1, deaths = $2, recoveries = $3 WHERE covid_state_name = $4',
        [input.confirmed_cases, input.deaths, input.recoveries, input.covid_state_name]);
        logger.info('updateOneCovid() query')
    
        let output = results.rows.map(convertToCovidArray);
    
        return output;
    }catch(err){
        console.log(err);
    }finally{
        client && client.release();
    }
    return[];
}

//Delete a user based on the full name AND password
export async function deleteOneUser(input:users){

    let client;
    try{
        client = await pool.connect();
        logger.info('Connected to database')
    
        await client.query('DELETE FROM users WHERE full_name = $1 AND user_password = $2',
        [input.full_name, input.user_password]);
        logger.info('deleteOneUser() query')
    
        return;
    }catch(err){
        console.log(err);
    }finally{
        client && client.release();
    }
    return;
}