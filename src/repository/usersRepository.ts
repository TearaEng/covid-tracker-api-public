//import everything needed for the user pipeline
import {logger} from '../middleware/winston'
import {users} from "../model/models"
import {pool} from "./database"
import {convertToUsersArray} from "../dto/DTO"
require('dotenv').config()

//Find all users by querying the database and convert it into an array of user objects
export async function findAllUsers(){
    let client;

    try{
        client = await pool.connect();
        logger.info('Connected to database')
    
        const results = await client.query('SELECT * FROM users');
        let output = results.rows.map(convertToUsersArray);
        logger.info('findAllUsers() query')
    
        return output;
    }catch(err){
        console.log(err);
    }finally{
        client && client.release();
    }
    return[];
}

//Create a new user and assign the id that is created by the database and return the object
export async function createNewUser(input:users){
    let client;
    let newUser = new users("","","","","","","",0,"");
    try{
        client = await pool.connect();
        logger.info('Connected to database')
    
        const results = await client.query('INSERT INTO users (full_name, user_role, user_password, user_state, health_status, address, email, phone_number) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING id;', 
        [input.full_name, input.user_role, input.user_password, input.user_state, input.health_status, input.address, input.email, input.phone_number]);
        logger.info(`createNewUser() query`)
        let newUserId = results.rows[0].id;
        logger.info(`The ID given by the database is ${newUserId}`)
        newUser = input;
        newUser.id = newUserId;
    
        return newUser;
    }catch(err){
        console.log(err);
    }finally{
        client && client.release();
    }
    return[];
}

//Return all users that live in the same state
export async function findByState(input:users){
    let client;
    try{
        client = await pool.connect();
        logger.info('Connected to database')
        
        const results = await client.query('SELECT full_name, user_role, user_state, health_status FROM users WHERE user_state = $1;', [input.user_state]);
        logger.info(`findByState() query`)
        let output = results.rows.map(convertToUsersArray);
    
        return output;
    }catch(err){
        console.log(err);
    }finally{
        client && client.release();
    }
    return[];
}