//import everything needed for the covid pipeline
import {logger} from '../middleware/winston'
import {covid} from "../model/models"
import {pool} from "./database"
require('dotenv').config()
import {convertToCovidArray} from "../dto/DTO"

//Find covid status based on given state name
export async function findOneCovid(input:covid){
    let client;
    try{
        client = await pool.connect();
        logger.info('Connected to database')
    
        const results = await client.query('SELECT * FROM covid WHERE covid_state_name = $1;', 
        [input.covid_state_name]);
        logger.info('findOneCovid() query')
        let output = results.rows.map(convertToCovidArray);
    
        return output;
    }catch(err){
        console.log(err);
    }finally{
        client && client.release();
    }
    return[];
}