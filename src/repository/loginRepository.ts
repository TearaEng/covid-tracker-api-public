//import everything needed for the login pipeline
import {logger} from '../middleware/winston'
import {users} from "../model/models"
import {pool} from "./database"
require('dotenv').config()

import {convertToUsersArray} from "../dto/DTO"

//Cross check with the database to make sure the given credentials are correct
export async function checkLogin(input:users){
    let client;
    let loginUser = new users("","","","","","","",0,"");
    try{
        client = await pool.connect();
        logger.info('Connected to database')
        
        const results = await client.query('SELECT * FROM users WHERE email = $1 AND user_password = $2',[input.email,input.user_password]);
        let loginEmail = results.rows[0].email
        let loginPassword = results.rows[0].user_password
        let loginRole = results.rows[0].user_role
        logger.info(`checkLogin() query has been successful`);
        
        //if the query is valid then return with login info and role
        loginUser.email = loginEmail
        loginUser.user_password = loginPassword
        loginUser.user_role = loginRole
    
        return loginUser;

    }catch(err){
        logger.info(`checkLogin() query has been unsuccessful`);
    }
    finally{
        client && client.release();
    }
    //If not then return an empty object
    logger.info(`Returning empty object`);
    return loginUser;
}