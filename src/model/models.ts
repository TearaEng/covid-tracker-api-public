export class users{
    full_name: string;
    user_role: string;
    user_password: string;
    user_state: string;
    health_status: string;
    address: string;
    email: string;
    id: number;
    phone_number: string;
    constructor(full_name: string, user_role: string, user_password: string, user_state: string, health_status: string, address: string, email: string, id: number, phone_number: string){
        this.full_name = full_name;
        this.user_role = user_role;
        this.user_password = user_password;
        this.user_state = user_state;
        this.health_status = health_status;
        this.address = address;
        this.email = email;
        this.id = id;
        this.phone_number = phone_number;
    }
}

export class states{
    state_name: string;
    population: number;
    state_size_sq_mi: number;
    density: number;
    constructor(state_name: string, population: number, state_size_sq_mi: number, density: number){
        this.state_name = state_name;
        this.population = population;
        this.state_size_sq_mi = state_size_sq_mi;
        this.density = density;
    }
}

export class covid{
    covid_state_name: string;
    confirmed_cases: number;
    deaths: number;
    recoveries: number;
    constructor(covid_state_name: string, confirmed_cases: number, deaths: number, recoveries: number){
        this.covid_state_name = covid_state_name;
        this.confirmed_cases = confirmed_cases;
        this.deaths = deaths;
        this.recoveries = recoveries;
    }
}