/*Import express to easily create a server and 
body-parser to be able to read the body of the request and
dotenv to have environmental variables*/
require('dotenv').config()
import express from 'express';
import * as bodyParser from 'body-parser';
import {logger} from './middleware/winston'
import cors from 'cors';
//Import all the routers to have the ability to stem from the same pattern
import {usersRouter} from './router/usersRouter'
import {covidRouter} from './router/covidRouter'
import {adminRouter} from './router/adminRouter'
import {loginRouter} from './router/loginRouter'

//open server
export const app = express();
const PORT = process.env.PORT || 5000;

//Confirmation that the server is up and running
module.exports = app.listen(PORT, ()=> {
    logger.info(`Server is running on port: ${PORT}`);
});

//middleware to get info from the body of the request
app.use(bodyParser.json())
app.use(cors());

//Call different functions based on the input given
app.use('/users', usersRouter);
app.use('/covid', covidRouter);
app.use('/auth', loginRouter);
app.use('/admin', adminRouter);

app.get('/', (req, res) => {

    res.send('Pipeline Works!!');

})