import {Router} from 'express';
import {logger} from '../middleware/winston'

//Import everything dealing with the user pipeline
import {users} from "../model/models"
import * as usersService from '../service/usersService'
export const usersRouter = Router();

//Get all users
usersRouter.get('/getAll', async (req, res) => {
    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);

    res.status(200).json(await usersService.findAllUsers());
    logger.info(`Returned to usersRouter`)
});

//Create a new user
usersRouter.post('/new', async (req, res) => {
    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);

    //parse the body and assign them to variables
    let {full_name, user_role, user_password, user_state, health_status, address, email, phone_number}:
    {full_name: string, user_role: string, user_password: string, user_state: string, health_status: string, address: string, email: string, phone_number: string} = req.body;
    //send the information to the repository and return a promise that is unwrapped in an array of objects
    let newUser = await usersService.createNewUser(new users(full_name, user_role, user_password, user_state, health_status, address, email, 0, phone_number))

    res.status(200).json(newUser);
    logger.info(`Returned to usersRouter`)
});

//Get users by state
usersRouter.post('/getByState', async (req, res) => {
    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);

    //only parse a the state to act a condition in the query
    let{user_state}:{user_state:string} = req.body;
    console.log(user_state);
    let userStates = await usersService.findByState(new users("","","",user_state,"","","",0,""))

    res.status(200).json(userStates);
    logger.info(`Returned to usersRouter`)
});