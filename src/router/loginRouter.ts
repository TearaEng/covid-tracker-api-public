import {Router} from 'express';
import {logger} from '../middleware/winston'

//Import everything dealing with the login pipeline
import {users} from '../model/models'
import * as loginService from '../service/loginService'
export const loginRouter = Router();

//JWT Import and creating a secret
import jwt from 'jsonwebtoken';
export const accessTokenSecret = "Lotte";
export const refreshTokenSecret = "Ghana";
export const refreshTokens: string[] = [];

loginRouter.post('/login', async (req, res)=>{
    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    try{
        
        logger.info('Session exists')
        //parse for email and password and send to cross check with the database
        let{email, user_password}:{email:string, user_password:string} = req.body;
        let validation = await loginService.checkLogin(new users("","", user_password,"","","", email,0,"",))
        logger.info(`returned to loginRouter`)

        //if the object returned is valid then assign it the object that will be exported
        if (validation.email == email && validation.user_password == user_password && validation.user_role == "Admin"){

            const accessToken = jwt.sign({
                username: validation.email,
                role: validation.user_role},
                accessTokenSecret,
                { expiresIn: '20m'}
            );

            const refreshToken = jwt.sign({
                username: validation.email,
                role: validation.user_role},
                refreshTokenSecret
            );

            refreshTokens.push(refreshToken);

            logger.info(`${validation.email} has logged in as an Admin`)
            res.json({accessToken, refreshToken});
        }
        //if the user is not a admin then they are a guest
        else if (validation.email == email && validation.user_password == user_password){

            const accessToken = jwt.sign({
                username: validation.email,
                role: validation.user_role},
                accessTokenSecret,
                { expiresIn: '20m'}
            );
            
            const refreshToken = jwt.sign({
                username: validation.email,
                role: validation.user_role},
                refreshTokenSecret
            );

            refreshTokens.push(refreshToken);

            logger.info(`${validation.email} has logged in as an Guest`)
            res.send({accessToken, refreshToken});
        }
        //else the wrong credentials were given
        else{
            logger.info(`Invalid Login`)
            res.send("Wrong credentials!");
        } 
    }catch(err){
        logger.info(`Database Error`)
        res.send("Database Problem")
    }
})

//logout the user
loginRouter.post('/logout',(req, res)=>{
    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);

    const {token} = req.body;
    let deletedTokens = refreshTokens.filter(t => t !== token);

    res.send("Logout successful");
})

//refresh token
loginRouter.post('/token', (req, res)=>{
    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    const {token} = req.body;

    if (!token){
        return res.status(401);
    }
    if (!refreshTokens.includes(token.refreshToken)) {
        return res.status(403);
    }

    jwt.verify(token.accessToken, refreshTokenSecret, (err, user) => {
        if (err) {
            return res.status(403);
        }

        token.accessToken = jwt.sign({ 
            username: user.username, 
            role: user.role }, 
            accessTokenSecret, 
            { expiresIn: '20m' }
        );

        logger.info(`Token has been refreshed`);
        res.json(token.accessToken);
    })
});