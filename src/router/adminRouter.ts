//Every router needs Router from express
import {Router} from 'express';
import {logger} from '../middleware/winston'

//Import everything dealing with the admin pipeline
import {users, covid} from '../model/models'
import * as adminService from '../service/adminService'
import {authenticateJWT} from '../middleware/auth'

//Export 
export const adminRouter = Router();

//Update the status of covid in a state
adminRouter.put('/updateOneCovid', authenticateJWT, async (req:any, res) =>{

    const user = req.user;
    if(user.role !== 'Admin'){
        return res.status(403).send('Not an Admin')
    }

    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    let {covid_state_name, confirmed_cases, deaths, recoveries}:{covid_state_name:string, confirmed_cases:number, deaths:number, recoveries:number} = req.body;
    await adminService.updateOneCovid(new covid(covid_state_name, confirmed_cases, deaths, recoveries));
    logger.info(`returned to adminRouter`)

    //let the user know that it's been updated
    res.status(200).send(`${covid_state_name} has been updated!`);
    logger.info(`${covid_state_name} COVID status has been changed`);
});

//Delete a user from the database based on its full name and password
adminRouter.delete('/deleteOneUser', authenticateJWT, async (req:any, res) =>{

    const user = req.user;
    if(user.role !== 'Admin'){
        return res.status(403).send('Not an Admin')
    }

    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    let {full_name,user_password}:{full_name:string, user_password:string} = req.body;
    await adminService.deleteOneUser(new users(full_name,"",user_password,"","","","",0,""));
    logger.info(`returned to adminRouter`)
    
    res.status(200).send(`${full_name} has been deleted!`);
    logger.info(`${full_name} has been deleted from the database`);
});