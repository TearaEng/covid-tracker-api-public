//Every router needs Router from express
import {Router} from 'express';
import {logger} from '../middleware/winston'

//Import everything dealing with the covid pipeline
import {covid} from "../model/models"
import * as covidService from '../service/covidService'
export const covidRouter = Router();

//get covid status based on a given state
covidRouter.post('/getOne', async (req, res) => {
    logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);

    let{covid_state_name}:{covid_state_name: string} = req.body;
    let covidState = await covidService.findOneCovid(new covid(covid_state_name, 0,0,0))
    
    res.status(200).json(covidState);
    logger.info(`Returned to covidRouter`)
});