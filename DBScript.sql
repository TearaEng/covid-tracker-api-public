CREATE TABLE states (
	state_name varchar(255) UNIQUE PRIMARY KEY,
	population int,
	state_size_sq_mi NUMERIC(10, 3),
	density NUMERIC(10, 3)
);

CREATE TABLE users (
	id serial PRIMARY KEY,
	full_name varchar(255),
	user_role varchar(255),
	user_password varchar(255),
	user_state varchar(255),
	health_status varchar(255),
	address varchar(255),
	phone_number varchar(10),
	email varchar(255) CHECK (email LIKE '%@%'),
	FOREIGN KEY (user_state) REFERENCES states(state_name)
);

CREATE TABLE covid (
	id serial PRIMARY KEY,
	covid_state_name varchar(255),
	confirmed_cases int,
	deaths int,
	recoveries int,
	FOREIGN KEY (covid_state_name) REFERENCES states(state_name)
);

INSERT INTO states (state_name, population, state_size_sq_mi, density)
 	VALUES ('New York', 8336817, 468.484, 27751),
 	('Virgina', 8535519, 42774.2, 206.7),
 	('Florida', 21477737, 65757.70, 384.3),
 	('Texas', 28995881, 268596, 108)

INSERT INTO users (full_name, user_role, user_password, user_state, health_status, address, phone_number, email)
	VALUES ('Timothy Hwoarang', 'Admin', 'talonkicks', 'New York', 'healthy', '8242 Sunbeam St. Granville, NY 12832', '7188724896', 'TimHwo@gmail.com'),
	('Ann Maki', 'Guest', 'worldModel', 'Virgina', 'healthy', '6308 Glenbard Rd. Burke, VA 22015', '7034405867', 'AnnModel@yahoo.com'),
	('Paul Pheonix','Guest','onePunch','Texas','infected', '404 S. Mayflower St. Dallas, TX 75248','4696524562', 'PaulPheo@yahoo.com'),
	('Teara Eng','Admin','GGStrive','Texas','healthy', '189 Bridle St. Austin, TX 78748','4698425462', 'teng@aol.com')
	
INSERT INTO covid (covid_state_name, confirmed_cases, deaths, recoveries)
	VALUES ('New York', 348232, 22478, 60796),
	('Virgina', 29683, 1002, 3909),
	('Florida', 43210, 1948, 18502),
	('Texas', 43851, 1216, 24487)