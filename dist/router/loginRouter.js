"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.refreshTokens = exports.refreshTokenSecret = exports.accessTokenSecret = exports.loginRouter = void 0;
const express_1 = require("express");
const winston_1 = require("../middleware/winston");
//Import everything dealing with the login pipeline
const models_1 = require("../model/models");
const loginService = __importStar(require("../service/loginService"));
exports.loginRouter = express_1.Router();
//JWT Import and creating a secret
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.accessTokenSecret = "Lotte";
exports.refreshTokenSecret = "Ghana";
exports.refreshTokens = [];
exports.loginRouter.post('/login', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    try {
        winston_1.logger.info('Session exists');
        //parse for email and password and send to cross check with the database
        let { email, user_password } = req.body;
        let validation = yield loginService.checkLogin(new models_1.users("", "", user_password, "", "", "", email, 0, ""));
        winston_1.logger.info(`returned to loginRouter`);
        //if the object returned is valid then assign it the object that will be exported
        if (validation.email == email && validation.user_password == user_password && validation.user_role == "Admin") {
            const accessToken = jsonwebtoken_1.default.sign({
                username: validation.email,
                role: validation.user_role
            }, exports.accessTokenSecret, { expiresIn: '20m' });
            const refreshToken = jsonwebtoken_1.default.sign({
                username: validation.email,
                role: validation.user_role
            }, exports.refreshTokenSecret);
            exports.refreshTokens.push(refreshToken);
            winston_1.logger.info(`${validation.email} has logged in as an Admin`);
            res.json({ accessToken, refreshToken });
        }
        //if the user is not a admin then they are a guest
        else if (validation.email == email && validation.user_password == user_password) {
            const accessToken = jsonwebtoken_1.default.sign({
                username: validation.email,
                role: validation.user_role
            }, exports.accessTokenSecret, { expiresIn: '20m' });
            const refreshToken = jsonwebtoken_1.default.sign({
                username: validation.email,
                role: validation.user_role
            }, exports.refreshTokenSecret);
            exports.refreshTokens.push(refreshToken);
            winston_1.logger.info(`${validation.email} has logged in as an Guest`);
            res.send({ accessToken, refreshToken });
        }
        //else the wrong credentials were given
        else {
            winston_1.logger.info(`Invalid Login`);
            res.send("Wrong credentials!");
        }
    }
    catch (err) {
        winston_1.logger.info(`Database Error`);
        res.send("Database Problem");
    }
}));
//logout the user
exports.loginRouter.post('/logout', (req, res) => {
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    const { token } = req.body;
    let deletedTokens = exports.refreshTokens.filter(t => t !== token);
    res.send("Logout successful");
});
//refresh token
exports.loginRouter.post('/token', (req, res) => {
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    const { token } = req.body;
    if (!token) {
        return res.status(401);
    }
    if (!exports.refreshTokens.includes(token.refreshToken)) {
        return res.status(403);
    }
    jsonwebtoken_1.default.verify(token.accessToken, exports.refreshTokenSecret, (err, user) => {
        if (err) {
            return res.status(403);
        }
        token.accessToken = jsonwebtoken_1.default.sign({
            username: user.username,
            role: user.role
        }, exports.accessTokenSecret, { expiresIn: '20m' });
        winston_1.logger.info(`Token has been refreshed`);
        res.json(token.accessToken);
    });
});
