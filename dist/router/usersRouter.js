"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.usersRouter = void 0;
const express_1 = require("express");
const winston_1 = require("../middleware/winston");
//Import everything dealing with the user pipeline
const models_1 = require("../model/models");
const usersService = __importStar(require("../service/usersService"));
exports.usersRouter = express_1.Router();
//Get all users
exports.usersRouter.get('/getAll', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    res.status(200).json(yield usersService.findAllUsers());
    winston_1.logger.info(`Returned to usersRouter`);
}));
//Create a new user
exports.usersRouter.post('/new', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    //parse the body and assign them to variables
    let { full_name, user_role, user_password, user_state, health_status, address, email, phone_number } = req.body;
    //send the information to the repository and return a promise that is unwrapped in an array of objects
    let newUser = yield usersService.createNewUser(new models_1.users(full_name, user_role, user_password, user_state, health_status, address, email, 0, phone_number));
    res.status(200).json(newUser);
    winston_1.logger.info(`Returned to usersRouter`);
}));
//Get users by state
exports.usersRouter.post('/getByState', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    //only parse a the state to act a condition in the query
    let { user_state } = req.body;
    console.log(user_state);
    let userStates = yield usersService.findByState(new models_1.users("", "", "", user_state, "", "", "", 0, ""));
    res.status(200).json(userStates);
    winston_1.logger.info(`Returned to usersRouter`);
}));
