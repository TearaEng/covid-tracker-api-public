"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.covidRouter = void 0;
//Every router needs Router from express
const express_1 = require("express");
const winston_1 = require("../middleware/winston");
//Import everything dealing with the covid pipeline
const models_1 = require("../model/models");
const covidService = __importStar(require("../service/covidService"));
exports.covidRouter = express_1.Router();
//get covid status based on a given state
exports.covidRouter.post('/getOne', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    let { covid_state_name } = req.body;
    let covidState = yield covidService.findOneCovid(new models_1.covid(covid_state_name, 0, 0, 0));
    res.status(200).json(covidState);
    winston_1.logger.info(`Returned to covidRouter`);
}));
