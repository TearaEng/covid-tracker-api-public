"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.adminRouter = void 0;
//Every router needs Router from express
const express_1 = require("express");
const winston_1 = require("../middleware/winston");
//Import everything dealing with the admin pipeline
const models_1 = require("../model/models");
const adminService = __importStar(require("../service/adminService"));
const auth_1 = require("../middleware/auth");
//Export 
exports.adminRouter = express_1.Router();
//Update the status of covid in a state
exports.adminRouter.put('/updateOneCovid', auth_1.authenticateJWT, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = req.user;
    if (user.role !== 'Admin') {
        return res.status(403).send('Not an Admin');
    }
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    let { covid_state_name, confirmed_cases, deaths, recoveries } = req.body;
    yield adminService.updateOneCovid(new models_1.covid(covid_state_name, confirmed_cases, deaths, recoveries));
    winston_1.logger.info(`returned to adminRouter`);
    //let the user know that it's been updated
    res.status(200).send(`${covid_state_name} has been updated!`);
    winston_1.logger.info(`${covid_state_name} COVID status has been changed`);
}));
//Delete a user from the database based on its full name and password
exports.adminRouter.delete('/deleteOneUser', auth_1.authenticateJWT, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = req.user;
    if (user.role !== 'Admin') {
        return res.status(403).send('Not an Admin');
    }
    winston_1.logger.info(`Request url is \'${req.url}\' and Request Method is ${req.method}`);
    let { full_name, user_password } = req.body;
    yield adminService.deleteOneUser(new models_1.users(full_name, "", user_password, "", "", "", "", 0, ""));
    winston_1.logger.info(`returned to adminRouter`);
    res.status(200).send(`${full_name} has been deleted!`);
    winston_1.logger.info(`${full_name} has been deleted from the database`);
}));
