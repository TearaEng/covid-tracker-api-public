"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkLogin = void 0;
//import everything needed for the login pipeline
const winston_1 = require("../middleware/winston");
const models_1 = require("../model/models");
const database_1 = require("./database");
require('dotenv').config();
//Cross check with the database to make sure the given credentials are correct
function checkLogin(input) {
    return __awaiter(this, void 0, void 0, function* () {
        let client;
        let loginUser = new models_1.users("", "", "", "", "", "", "", 0, "");
        try {
            client = yield database_1.pool.connect();
            winston_1.logger.info('Connected to database');
            const results = yield client.query('SELECT * FROM users WHERE email = $1 AND user_password = $2', [input.email, input.user_password]);
            let loginEmail = results.rows[0].email;
            let loginPassword = results.rows[0].user_password;
            let loginRole = results.rows[0].user_role;
            winston_1.logger.info(`checkLogin() query has been successful`);
            //if the query is valid then return with login info and role
            loginUser.email = loginEmail;
            loginUser.user_password = loginPassword;
            loginUser.user_role = loginRole;
            return loginUser;
        }
        catch (err) {
            winston_1.logger.info(`checkLogin() query has been unsuccessful`);
        }
        finally {
            client && client.release();
        }
        //If not then return an empty object
        winston_1.logger.info(`Returning empty object`);
        return loginUser;
    });
}
exports.checkLogin = checkLogin;
