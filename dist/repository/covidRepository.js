"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOneCovid = void 0;
//import everything needed for the covid pipeline
const winston_1 = require("../middleware/winston");
const database_1 = require("./database");
require('dotenv').config();
const DTO_1 = require("../dto/DTO");
//Find covid status based on given state name
function findOneCovid(input) {
    return __awaiter(this, void 0, void 0, function* () {
        let client;
        try {
            client = yield database_1.pool.connect();
            winston_1.logger.info('Connected to database');
            const results = yield client.query('SELECT * FROM covid WHERE covid_state_name = $1;', [input.covid_state_name]);
            winston_1.logger.info('findOneCovid() query');
            let output = results.rows.map(DTO_1.convertToCovidArray);
            return output;
        }
        catch (err) {
            console.log(err);
        }
        finally {
            client && client.release();
        }
        return [];
    });
}
exports.findOneCovid = findOneCovid;
