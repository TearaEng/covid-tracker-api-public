"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findByState = exports.createNewUser = exports.findAllUsers = void 0;
//import everything needed for the user pipeline
const winston_1 = require("../middleware/winston");
const models_1 = require("../model/models");
const database_1 = require("./database");
const DTO_1 = require("../dto/DTO");
require('dotenv').config();
//Find all users by querying the database and convert it into an array of user objects
function findAllUsers() {
    return __awaiter(this, void 0, void 0, function* () {
        let client;
        try {
            client = yield database_1.pool.connect();
            winston_1.logger.info('Connected to database');
            const results = yield client.query('SELECT * FROM users');
            let output = results.rows.map(DTO_1.convertToUsersArray);
            winston_1.logger.info('findAllUsers() query');
            return output;
        }
        catch (err) {
            console.log(err);
        }
        finally {
            client && client.release();
        }
        return [];
    });
}
exports.findAllUsers = findAllUsers;
//Create a new user and assign the id that is created by the database and return the object
function createNewUser(input) {
    return __awaiter(this, void 0, void 0, function* () {
        let client;
        let newUser = new models_1.users("", "", "", "", "", "", "", 0, "");
        try {
            client = yield database_1.pool.connect();
            winston_1.logger.info('Connected to database');
            const results = yield client.query('INSERT INTO users (full_name, user_role, user_password, user_state, health_status, address, email, phone_number) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING id;', [input.full_name, input.user_role, input.user_password, input.user_state, input.health_status, input.address, input.email, input.phone_number]);
            winston_1.logger.info(`createNewUser() query`);
            let newUserId = results.rows[0].id;
            winston_1.logger.info(`The ID given by the database is ${newUserId}`);
            newUser = input;
            newUser.id = newUserId;
            return newUser;
        }
        catch (err) {
            console.log(err);
        }
        finally {
            client && client.release();
        }
        return [];
    });
}
exports.createNewUser = createNewUser;
//Return all users that live in the same state
function findByState(input) {
    return __awaiter(this, void 0, void 0, function* () {
        let client;
        try {
            client = yield database_1.pool.connect();
            winston_1.logger.info('Connected to database');
            const results = yield client.query('SELECT full_name, user_role, user_state, health_status FROM users WHERE user_state = $1;', [input.user_state]);
            winston_1.logger.info(`findByState() query`);
            let output = results.rows.map(DTO_1.convertToUsersArray);
            return output;
        }
        catch (err) {
            console.log(err);
        }
        finally {
            client && client.release();
        }
        return [];
    });
}
exports.findByState = findByState;
