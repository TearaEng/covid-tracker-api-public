"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteOneUser = exports.updateOneCovid = void 0;
//import everything needed for the admin pipeline
const winston_1 = require("../middleware/winston");
const database_1 = require("./database");
const DTO_1 = require("../dto/DTO");
require('dotenv').config();
//Update covid status 
function updateOneCovid(input) {
    return __awaiter(this, void 0, void 0, function* () {
        let client;
        try {
            client = yield database_1.pool.connect();
            winston_1.logger.info('Connected to database');
            const results = yield client.query('UPDATE covid SET confirmed_cases = $1, deaths = $2, recoveries = $3 WHERE covid_state_name = $4', [input.confirmed_cases, input.deaths, input.recoveries, input.covid_state_name]);
            winston_1.logger.info('updateOneCovid() query');
            let output = results.rows.map(DTO_1.convertToCovidArray);
            return output;
        }
        catch (err) {
            console.log(err);
        }
        finally {
            client && client.release();
        }
        return [];
    });
}
exports.updateOneCovid = updateOneCovid;
//Delete a user based on the full name AND password
function deleteOneUser(input) {
    return __awaiter(this, void 0, void 0, function* () {
        let client;
        try {
            client = yield database_1.pool.connect();
            winston_1.logger.info('Connected to database');
            yield client.query('DELETE FROM users WHERE full_name = $1 AND user_password = $2', [input.full_name, input.user_password]);
            winston_1.logger.info('deleteOneUser() query');
            return;
        }
        catch (err) {
            console.log(err);
        }
        finally {
            client && client.release();
        }
        return;
    });
}
exports.deleteOneUser = deleteOneUser;
