"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.convertToCovidArray = exports.convertToStatesArray = exports.convertToUsersArray = exports.covidDTO = exports.statesDTO = exports.usersDTO = void 0;
const models_1 = require("../model/models");
class usersDTO {
    constructor(full_name, user_role, user_password, user_state, health_status, address, email, id, phone_number) {
        this.full_name = full_name;
        this.user_role = user_role;
        this.user_password = user_password;
        this.user_state = user_state;
        this.health_status = health_status;
        this.address = address;
        this.email = email;
        this.id = id;
        this.phone_number = phone_number;
    }
}
exports.usersDTO = usersDTO;
class statesDTO {
    constructor(state_name, population, state_size_sq_mi, density) {
        this.state_name = state_name;
        this.population = population;
        this.state_size_sq_mi = state_size_sq_mi;
        this.density = density;
    }
}
exports.statesDTO = statesDTO;
class covidDTO {
    constructor(covid_state_name, confirmed_cases, deaths, recoveries) {
        this.covid_state_name = covid_state_name;
        this.confirmed_cases = confirmed_cases;
        this.deaths = deaths;
        this.recoveries = recoveries;
    }
}
exports.covidDTO = covidDTO;
//Used to convert objects from the database to objects in the server
function convertToUsersArray(input) {
    const newUsers = new models_1.users(input.full_name, input.user_role, input.user_password, input.user_state, input.health_status, input.address, input.email, input.id, input.phone_number);
    return newUsers;
}
exports.convertToUsersArray = convertToUsersArray;
function convertToStatesArray(input) {
    const newStates = new models_1.states(input.state_name, input.population, input.state_size_sq_mi, input.density);
    return newStates;
}
exports.convertToStatesArray = convertToStatesArray;
function convertToCovidArray(input) {
    const newCovid = new models_1.covid(input.covid_state_name, input.confirmed_cases, input.deaths, input.recoveries);
    return newCovid;
}
exports.convertToCovidArray = convertToCovidArray;
