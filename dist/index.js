"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
/*Import express to easily create a server and
body-parser to be able to read the body of the request and
dotenv to have environmental variables*/
require('dotenv').config();
const express_1 = __importDefault(require("express"));
const bodyParser = __importStar(require("body-parser"));
const winston_1 = require("./middleware/winston");
const cors_1 = __importDefault(require("cors"));
//Import all the routers to have the ability to stem from the same pattern
const usersRouter_1 = require("./router/usersRouter");
const covidRouter_1 = require("./router/covidRouter");
const adminRouter_1 = require("./router/adminRouter");
const loginRouter_1 = require("./router/loginRouter");
//open server
exports.app = express_1.default();
const PORT = process.env.PORT || 5000;
//Confirmation that the server is up and running
module.exports = exports.app.listen(PORT, () => {
    winston_1.logger.info(`Server is running on port: ${PORT}`);
});
//middleware to get info from the body of the request
exports.app.use(bodyParser.json());
exports.app.use(cors_1.default());
//Call different functions based on the input given
exports.app.use('/users', usersRouter_1.usersRouter);
exports.app.use('/covid', covidRouter_1.covidRouter);
exports.app.use('/auth', loginRouter_1.loginRouter);
exports.app.use('/admin', adminRouter_1.adminRouter);
exports.app.get('/', (req, res) => {
    res.send('Pipeline Works!!');
});
