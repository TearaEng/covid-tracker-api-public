"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sessionMid = void 0;
const express_session_1 = __importDefault(require("express-session"));
//Set the secret and export the session
const sessionConfig = { secret: 'Lotte' };
exports.sessionMid = express_session_1.default(sessionConfig);
