"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.covid = exports.states = exports.users = void 0;
class users {
    constructor(full_name, user_role, user_password, user_state, health_status, address, email, id, phone_number) {
        this.full_name = full_name;
        this.user_role = user_role;
        this.user_password = user_password;
        this.user_state = user_state;
        this.health_status = health_status;
        this.address = address;
        this.email = email;
        this.id = id;
        this.phone_number = phone_number;
    }
}
exports.users = users;
class states {
    constructor(state_name, population, state_size_sq_mi, density) {
        this.state_name = state_name;
        this.population = population;
        this.state_size_sq_mi = state_size_sq_mi;
        this.density = density;
    }
}
exports.states = states;
class covid {
    constructor(covid_state_name, confirmed_cases, deaths, recoveries) {
        this.covid_state_name = covid_state_name;
        this.confirmed_cases = confirmed_cases;
        this.deaths = deaths;
        this.recoveries = recoveries;
    }
}
exports.covid = covid;
